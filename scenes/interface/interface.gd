extends Control

func _input(event):
    if event.is_action_pressed('ui_toggle_pause'):
        get_tree().paused = not get_tree().paused

    if event.is_action_pressed('ui_toggle_ui'):
        self.toggle_control('Ui')
    if event.is_action_pressed('ui_toggle_all'):
        self.toggle_control('All')
    if event.is_action_pressed('ui_toggle_forces'):
        self.toggle_control('Forces')
    if event.is_action_pressed('ui_toggle_targets'):
        self.toggle_control('Targets')

func toggle_ui():
   $Controls.visible = not $Controls.visible

func toggle_control(control):
    match control:
        'Ui':
            self.toggle_ui()
            var node = $Controls/Ui
            node.toggle()
        'All':
            var node = $Controls/All
            node.toggle()
            if node.is_pressed:
                self.press_control('Ui')
                self.press_control('Forces')
                self.press_control('Targets')
                $Controls.visible = false
            else:
                self.release_control('Ui')
                self.release_control('Forces')
                self.release_control('Targets')
                $Controls.visible = true
        'Forces':
            $Forces.display_forces = not $Forces.display_forces
            var node = $Controls/Forces
            node.toggle()
        'Targets':
            $Forces.display_targets = not $Forces.display_targets
            var node = $Controls/Targets
            node.toggle()

func press_control(control):
      match control:
        'Ui':
            $Controls.visible = false
            var node = $Controls/Ui
            node.press()
        'Forces':
            $Forces.display_forces = false
            var node = $Controls/Forces
            node.press()
        'Targets':
            $Forces.display_targets = false
            var node = $Controls/Targets
            node.press()

func release_control(control):
      match control:
        'Ui':
            $Controls.visible = true
            var node = $Controls/Ui
            node.release()
        'Forces':
            $Forces.display_forces = true
            var node = $Controls/Forces
            node.release()
        'Targets':
            $Forces.display_targets = true
            var node = $Controls/Targets
            node.release()
