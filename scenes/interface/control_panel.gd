extends Panel

export var displayed_key = ''
export var displayed_text = ''

var is_pressed = false

func _ready():
    $Margin/HBox/Key.text = self.displayed_key
    $Margin/HBox/Label.text = self.displayed_text

func toggle():
    self.is_pressed = not self.is_pressed
    self._toggle_color(self.is_pressed)

func press():
    self.is_pressed = true
    var node = $Margin/HBox/Label
    var color = node.get_color('font_color')
    node.add_color_override('font_color', Color(color.r, color.g, color.b, 0.35))

func release():
    self.is_pressed = false
    var node = $Margin/HBox/Label
    var color = node.get_color('font_color')
    node.add_color_override('font_color', Color(color.r, color.g, color.b, 1))

func _toggle_color(pressed):
    var node = $Margin/HBox/Label
    var color = node.get_color('font_color')

    if pressed == true:
        node.add_color_override('font_color', Color(color.r, color.g, color.b, 0.35))
    else:
        node.add_color_override('font_color', Color(color.r, color.g, color.b, 1))