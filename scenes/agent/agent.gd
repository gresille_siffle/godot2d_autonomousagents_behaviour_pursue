# Steering behavior
# Pursue

extends KinematicBody2D

export (int) var mass = 4
export (int) var max_force = 10
export (int) var max_speed = 380

var velocity = Vector2()
var acceleration = Vector2()

var steering_force = Vector2()
var desired_velocity = Vector2()

var slowing_distance = 120

var prey = null
export (bool) var is_predator = false

func _ready():
    if self.is_predator:
        self.prey = get_node('/root/Main/Agents/Prey')
        $Sprite.texture = preload('res://art/agent_red.png')

func _physics_process(delta):
    if self.is_predator:
        self.pursue(self.prey)
    else:
        self.seek_and_arrive(self._get_mouse_position())
    self.move()

func seek(target_position):
    self.desired_velocity = (target_position - self.position).normalized() * self.max_speed
    self.steering_force = (self.desired_velocity - self.velocity).clamped(self.max_force)
    self.apply_force(self.steering_force)

func seek_with_mass(target_position):
    self.desired_velocity = (target_position - self.position).normalized() * self.max_speed
    self.steering_force = (self.desired_velocity - self.velocity) / self.mass
    self.apply_force(self.steering_force)

func seek_and_arrive(target_position):
    var desired_velocity = (target_position - self.position)

    var target_distance = desired_velocity.length()
    desired_velocity = desired_velocity.normalized() * self.max_speed
    self.desired_velocity = self.approach_target(self.slowing_distance, target_distance, desired_velocity)

    self.steering_force = (self.desired_velocity - self.velocity).clamped(self.max_force)
    self.apply_force(self.steering_force)

func approach_target(slowing_distance, distance_to_target, velocity):
    if distance_to_target < slowing_distance:
        velocity *= (distance_to_target / slowing_distance)
    return velocity

func pursue(target):
    var distance = self.position.distance_to(target.position)
    var anticipation = distance / self.max_speed
    var future_target_position = target.position + target.velocity * anticipation
    self.seek(future_target_position)

func move():
    self.velocity += self.acceleration
    self.velocity = self.velocity.clamped(self.max_speed)
    self.rotation = self.velocity.angle() + PI / 2
    self.move_and_slide(self.velocity)
    self.acceleration = Vector2()

func apply_force(force):
    self.acceleration += force

func _get_mouse_position():
    return get_viewport().get_mouse_position()
